#---------------------------------------------------------------------------------------------------------------------------------------
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#---------------------------------------------------------------------------------------------------------------------------------------
/**
 * Arquitectura y Dise�o de Sistemas
 * Primer cuatrimestre 2017
 * Comisi�n 2
 * API RSS 
 * @author Barreix, I�aki Salvador LU: 108 998
 * @author Boruta, Daiana Mar�a LU: 99 858
 * @author Pandolfi, Manuel LU: 108 597
 */
 
La API brinda los siguientes m�todos en su interfaz p�blica MainRSS, la cual va a ser la que no permita cargar y obtener datos:
		+getInstance():MainRSS 
+getModuloURLs():URLContainer 
+getModuloNews():NewsManager
	
Para poder usar estos m�todos se debe importar el paquete:
import rssfeed.api.logic.MainRSS;
import rssfeed.api.model.*;
	
	Los m�todos mencionados est�n disponibles en la interfaz llamada: MainRSS.
 
	IMPORTANTE: Recordar tener el .jar de la API en el Build Path de su proyecto.
		
#---------------------------------------------------------------------------------------------------------------------------------------
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#---------------------------------------------------------------------------------------------------------------------------------------
	
El uso de la API ser� a trav�s de la interfaz mencionada y se tienen que seguir los siguientes pasos para realizar un uso correcto de la misma.
 
Crear una instancia de MainRSS.
Para agregar o quitar una o m�s url, pedir al MainRSS el m�dulo URLContainer y hacer uso de los m�todos que brinda ( addURL(string: url) o removeURL(string url) ). (Opcional: si se quiere quitar o agregar m�s de una url repetir este paso con las diferentes url�s).
 		* Importante: La url debe tener contenido rss ejemplo: �http://www.rollingstone.com/rss�.
Para obtener las noticias, pedir al MainRSS el m�dulo NewsManager a trav�s del cual se puede acceder al m�todo getNews(), que devuelve un arreglo de Feed_RSS. De no existir al menos una url cargada en el m�dulo URLContainer (paso 2), el m�todo devolver� una excepci�n de tipo EmptyURLException.
	
#---------------------------------------------------------------------------------------------------------------------------------------
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#---------------------------------------------------------------------------------------------------------------------------------------
Ejemplo de uso:
	
import rssfeed.api.logic.MainRSS;
import rssfeed.api.model.*;
 
public class AplicacionQueUsaApiRSS {
 
MainRSS mainRSS = MainRSS.getInstance();	 //solicito al conector la instancia de la api
mainRSS.getModuloURLs().addURL(�urlejemplo.com/rss�); //a�ado una url a la api 
		
		try {
			Feed_RSS[] feedsApi = mainRSS.getModuloNews().getNews();
			// consulta a la api sobre las noticias y esta se las devuelve correctamente un arreglo Feed_RSS.
		}
		catch(EmptyUrlException e) {
			//c�digo del manejo de la excepci�n en caso de no haber urls en el URLContainer
		}
}
