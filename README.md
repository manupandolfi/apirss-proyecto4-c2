# README #

La **API RSS** brinda los siguientes métodos en su interfaz pública MainRSS, la cual va a ser la que no permita cargar y obtener datos:

```
#!java

+getInstance():MainRSS 
+getModuloURLs():URLContainer 
+getModuloNews():NewsManager
```

### Configuracion de la API ###

Para poder usar estos métodos se debe importar el paquete:

```
#!java

import rssfeed.api.logic.MainRSS;
import rssfeed.api.model.*;

```
	
Los métodos mencionados están disponibles en la interfaz llamada: MainRSS.          
**IMPORTANTE:** Recordar tener el .jar de la API en el Build Path de su proyecto.


### Uso de la API ###

*El uso de la API será a través de la interfaz mencionada y se tienen que seguir los siguientes pasos para realizar un uso correcto de la misma.*
 
* Crear una instancia de MainRSS.

* Para agregar o quitar una o más url, pedir al MainRSS el módulo URLContainer y hacer uso de los métodos que brinda ( *addURL(string: url) o removeURL(string url)* ). (Opcional: si se quiere quitar o agregar más de una url repetir este paso con las diferentes url´s).   
** IMPORTANTE:** La url debe tener contenido rss ejemplo: ‘[http://www.rollingstone.com/rss](Link URL)’.

* Para obtener las noticias, pedir al MainRSS el módulo NewsManager a través del cual se puede acceder al método getNews(), que devuelve un arreglo de Feed_RSS. De no existir al menos una url cargada en el módulo URLContainer (paso 2), el método devolverá una excepción de tipo EmptyURLException.
	


### Ejemplo de uso ###


```
#!java

import rssfeed.api.logic.MainRSS;
import rssfeed.api.model.*;
 
public class AplicacionQueUsaApiRSS {
 
MainRSS mainRSS = MainRSS.getInstance();	 //solicito al conector la instancia de la api
mainRSS.getModuloURLs().addURL(“urlejemplo.com/rss”); //añado una url a la api 
		
		try {
			Feed_RSS[] feedsApi = mainRSS.getModuloNews().getNews();
			// consulta a la api sobre las noticias y esta se las devuelve correctamente un arreglo Feed_RSS.
		}
		catch(EmptyUrlException e) {
			//código del manejo de la excepción en caso de no haber urls en el URLContainer
		}
}
```



### Acerca de ###

 * Arquitectura y Diseño de Sistemas
 * Primer cuatrimestre 2017
 * Comisión 2
 * API RSS 
 * Autores
    1. @isbarreix Barreix, Iñaki Salvador LU: 108 998
    2. @daimb Boruta, Daiana María LU: 99 858
    3. @manupandolfi Pandolfi, Manuel LU: 108 597