package rssfeed.api.model;

/**
* Excepci�n EmptyURLException.
* Modela una excepci�n para el caso en el que no se hayan cargado URLs en la API.
* 
* Proyecto parte 4 - API
* Arquitectura y Dise�o de Sistemas
* Primer cuatrimestre 2017
* Comisi�n 2
* @author Barreix, I�aki Salvador LU: 108 998
* @author Boruta, Daiana Mar�a LU: 99 858
* @author Pandolfi, Manuel LU: 108 597
*/
public class EmptyUrlException extends Exception {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor de la exepci�n
	 * @param s String describiendo la excepci�n
	 */
	public EmptyUrlException(String s) {
		
		super(s);
	}
}
