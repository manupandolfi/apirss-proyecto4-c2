package rssfeed.api.model;

/**
* Clase Feed_RSS. Modela un objeto noticia.
* Almacena el t�tulo y el link correspondiente a la noticia.
* 
* Proyecto parte 4 - API
* Arquitectura y Dise�o de Sistemas
* Primer cuatrimestre 2017
* Comisi�n 2
* @author Barreix, I�aki Salvador LU: 108 998
* @author Boruta, Daiana Mar�a LU: 99 858
* @author Pandolfi, Manuel LU: 108 597
*/
public class Feed_RSS {
	
	private String title, link;
	
	/**
	 * Constructor
	 * @param link URL de la noticia
	 * @param title T�tulo de la noticia
	 */
	public Feed_RSS(String link, String title) {

		this.title = title;
		this.link = link;
	}

	/**
	 * Getter de la URL de la noticia.
	 * @return String conteniendo la URL de la noticia
	 */
	public String getLink() {
		
		return link;
	}

	/**
	 * Setter de la URL de la noticia.
	 * @param link String conteniendo la nueva URL de la noticia
	 */
	public void setLink(String link) {
		
		this.link = link;
	}

	/**
	 * Getter del t�tulo de la noticia.
	 * @return String conteniendo el t�tulo de la noticia
	 */
	public String getTitle() {
		
		return title;
	}

	/**
	 * Setter del t�tulo de la noticia.
	 * @param title String conteniendo el nuevo t�tulo de la noticia
	 */
	public void setTitle(String title) {
		
		this.title = title;
	}	
}