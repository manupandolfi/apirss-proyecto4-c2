package rssfeed.api.logic;

/**
 * Clase MainRSS. Punto de entrada a la API.
 * Se encarga de proveer objetos URLContainer y NewsManager.
 * Implementa el patr�n Singleton.
 * 
 * Proyecto parte 4 - API
 * Arquitectura y Dise�o de Sistemas
 * Primer cuatrimestre 2017
 * Comisi�n 2
 * @author Barreix, I�aki Salvador
 * @author Boruta, Daiana Mar�a
 * @author Pandolfi, Manuel
 */
public class MainRSS {
	
	private static MainRSS instance;
	private URLContainer urlContainer;
	private NewsManager newsManager;
	
	/**
	 * Constructor privado.
	 */
	private MainRSS() {
		
		urlContainer = new URLContainerLista();
		newsManager = new NewsManagerImpl((URLDelivery) urlContainer);
	}
	
	/**
	 * M�todo por el que se obtiene la �nica instancia posible de MainRSS.
	 * De ser la primera vez que se llama al m�todo, se crea una nueva instancia.
	 * @return Instancia de MainRSS.
	 */
	public static MainRSS getInstance() {
		
		if (instance == null)
			instance = new MainRSS();
		return instance;
	}
	
	/**
	 * Getter del objeto de tipo URLContainer
	 * @return Instancia concreta de un URLContainer
	 */
	public URLContainer getModuloURLs(){
		
		return urlContainer;
	}
	
	/**
	 * Getter del objeto de tipo NewsManager
	 * @return Instancia concreta de un NewsManager
	 */
	public NewsManager getModuloNews(){
		
		return newsManager;
	}
	
}
