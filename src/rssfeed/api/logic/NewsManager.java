package rssfeed.api.logic;

import rssfeed.api.model.EmptyUrlException;
import rssfeed.api.model.Feed_RSS;

/**
 * Interfaz NewsManager. Expone m�todos de obtenci�n de noticias.
 * 
 * Proyecto parte 4 - API
 * Arquitectura y Dise�o de Sistemas
 * Primer cuatrimestre 2017
 * Comisi�n 2
 * @author Barreix, I�aki Salvador
 * @author Boruta, Daiana Mar�a
 * @author Pandolfi, Manuel
 */
public interface NewsManager {

	/**
	 * M�todo para la obtenci�n de noticias.
	 * @return Arreglo de Feed_RSS conteniendo las noticias encontradas
	 * 		   en base a las URLs cargadas en la API.
	 * @throws EmptyUrlException en caso de no existir URLs cargadas.
	 */
	public Feed_RSS[] getNews() throws EmptyUrlException;

}
