package rssfeed.api.logic;

import java.util.LinkedList;
import java.util.List;

/**
 * Clase URLContainerLista. Implementa las interfaces URLContainer y URLDelivery.
 * Almacena las URLs en una lista enlazada provista por Java.
 * 
 * Proyecto parte 4 - API
 * Arquitectura y Dise�o de Sistemas
 * Primer cuatrimestre 2017
 * Comisi�n 2
 * @author Barreix, I�aki Salvador
 * @author Boruta, Daiana Mar�a
 * @author Pandolfi, Manuel
 */
class URLContainerLista implements URLContainer, URLDelivery {

	private List<String> URLs;
	
	/**
	 * Constructor de la clase.
	 * Inicializa la lista enlazada.
	 */
	public URLContainerLista() {
		
		URLs = new LinkedList<String>();
	}
	
	/**
	 * Agrega una URL al container
	 * @param URL String conteniendo la URL
	 * @return
	 * 		true si la adici�n fue exitosa
	 * 		false en caso contrario
	 */
	public boolean addURL(String URL) {
		
		return URLs.add(URL);
	}

	/**
	 * Elimina una URL del container
	 * @param URL String de la URL a eliminar
	 * @return
	 * 		true si la remoci�n fue exitosa
	 * 		false en caso contrario
	 */
	public boolean removeURL(String URL) {
		
		return URLs.remove(URL);
	}

	/**
	 * M�todo para la obtenci�n de un iterable de todas las URLs almacenadas en el container.
	 * @return Objeto de tipo Iterable conteniendo las URLs.
	 */
	public Iterable<String> getURLs() {
		
		return URLs;
	}
}
