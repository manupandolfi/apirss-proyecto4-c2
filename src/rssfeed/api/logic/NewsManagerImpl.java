package rssfeed.api.logic;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import rssfeed.api.model.EmptyUrlException;
import rssfeed.api.model.Feed_RSS;

/**
 * Clase NewsManagerImpl. Implementa la interfaz NewsManager.
 * Implementa la l�gica de obtener noticias en formato xml rss de una url espec�fica.
 * Parsea el xml y se construye un arreglo de objetos Feed_RSS a partir del mismo.
 * 
 * Proyecto parte 4 - API
 * Arquitectura y Dise�o de Sistemas
 * Primer cuatrimestre 2017
 * Comisi�n 2
 * @author Barreix, I�aki Salvador
 * @author Boruta, Daiana Mar�a
 * @author Pandolfi, Manuel
 */
class NewsManagerImpl implements NewsManager{

	private URLDelivery urlDelivery;
	
	/**
	 * Constructor de la clase
	 * @param urls URLContainer del que ser�n obtenidas las URLs para generar los objetos Feed_RSS.
	 */
	public NewsManagerImpl(URLDelivery urls) {
		
		urlDelivery = urls;
	}
	
	/**
	 * M�todo para la obtenci�n de noticias.
	 * @return Arreglo de Feed_RSS conteniendo las noticias encontradas
	 * 		   en base a las URLs cargadas en la API.
	 * @throws EmptyUrlException en caso de no existir URLs cargadas.
	 */
	public Feed_RSS[] getNews() throws EmptyUrlException{
		
		Iterable<String> urls = urlDelivery.getURLs();
		Iterator<String> iterador = urls.iterator();
		
		if(!iterador.hasNext())
			throw new EmptyUrlException("No se ha seteado ninguna URL.");
		
		try {
			
			List<Feed_RSS> listFeeds = new LinkedList<Feed_RSS>();
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = null;
			db = dbf.newDocumentBuilder();
			
			for(String s: urls) {
				
				URL url = new URL(s);
				
				Document doc = db.parse(url.openStream());
				NodeList items = doc.getDocumentElement().getElementsByTagName("item");
				
				for(int i = 0; i < items.getLength(); i++) {
					
					Element item = (Element)items.item(i);
					Element title = (Element)item.getElementsByTagName("title").item(0);
					Element link = (Element)item.getElementsByTagName("link").item(0);

					Feed_RSS f = new Feed_RSS(link.getTextContent(), title.getTextContent());
			
					listFeeds.add(f);
				}
			}
			
			Feed_RSS[] feeds = listFeeds.toArray(new Feed_RSS[0]);
		
			return feeds;	
		} catch (SAXException e) {
			
			JOptionPane.showMessageDialog(null, "La URL ingresada es incorrecta.", "Error URL", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			
			JOptionPane.showMessageDialog(null, "La URL ingresada es incorrecta.", "Error URL", JOptionPane.ERROR_MESSAGE);
		} catch (ParserConfigurationException e1) {
			// handle ParserConfigurationException
		}
		
		return null;
	}
}
