package rssfeed.api.logic;

/**
 * Interfaz URLContainer.
 * Expone m�todos para a�adir, remover y obtener URLs de las cuales luego se obtendr�n las noticias.
 * 
 * Proyecto parte 4 - API
 * Arquitectura y Dise�o de Sistemas
 * Primer cuatrimestre 2017
 * Comisi�n 2
 * @author Barreix, I�aki Salvador
 * @author Boruta, Daiana Mar�a
 * @author Pandolfi, Manuel
 */
public interface URLContainer {

	/**
	 * Agrega una URL al container
	 * @param URL String conteniendo la URL
	 * @return
	 * 		true si la adici�n fue exitosa
	 * 		false en caso contrario
	 */
	public boolean addURL(String URL);
	
	/**
	 * Elimina una URL del container
	 * @param URL String de la URL a eliminar
	 * @return
	 * 		true si la remoci�n fue exitosa
	 * 		false en caso contrario
	 */
	public boolean removeURL(String URL);
}
