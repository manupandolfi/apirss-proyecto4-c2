package rssfeed.api.logic;

/**
 * Interfaz URLDelivery.
 * Expone m�todos para la obtenci�n de un iterable de URLs.
 * Complementa a la interfaz URLContainer, permitiendo que los m�todos provistos
 * sean de visibilidad paquete.
 * 
 * Proyecto parte 4 - API
 * Arquitectura y Dise�o de Sistemas
 * Primer cuatrimestre 2017
 * Comisi�n 2
 * @author Barreix, I�aki Salvador
 * @author Boruta, Daiana Mar�a
 * @author Pandolfi, Manuel
 */
interface URLDelivery {

	/**
	 * M�todo para la obtenci�n de un iterable de todas las URLs almacenadas en el container.
	 * @return Objeto de tipo Iterable conteniendo las URLs.
	 */
	Iterable<String> getURLs();
}
